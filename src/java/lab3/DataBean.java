/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Beans/Bean.java to edit this template
 */
package lab3;

import java.beans.*;
import java.io.Serializable;
import java.util.Random;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author baro
 */
public class DataBean implements Serializable {
    
    public static final String PROP_PROPERTY = "0";
    private static String history_file = "/tmp/.jeet_lab3_history.xml";
    private static List<Result> results_list = new ArrayList<>();
    
    private String a_str;
    private String b_str;
    private String c_str;
    private String x_str;
    private String y_str;
    
    private PropertyChangeSupport propertySupport;
    
    
    public DataBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    
    public String getA_str() {
        return a_str;
    }
    
    public String getB_str() {
        return b_str;
    }
    
    public String getC_str() {
        return c_str;
    }
    
    public String getX_str() {
        return x_str;
    }
    
    public String getY_str() {
        return y_str;
    }
    
    
    public void setA_str(String value) {
        String oldValue = a_str;
        a_str = value;
        propertySupport.firePropertyChange(PROP_PROPERTY, oldValue, a_str);
    }
    
    public void setB_str(String value) {
        String oldValue = b_str;
        b_str = value;
        propertySupport.firePropertyChange(PROP_PROPERTY, oldValue, b_str);
    }
    
    public void setC_str(String value) {
        String oldValue = c_str;
        c_str = value;
        propertySupport.firePropertyChange(PROP_PROPERTY, oldValue, c_str);
    }
    
    public void setX_str(String value) {
        String oldValue = x_str;
        x_str = value;
        propertySupport.firePropertyChange(PROP_PROPERTY, oldValue, x_str);
    }
    
    public void setY_str(String value) {
        String oldValue = y_str;
        y_str = value;
        propertySupport.firePropertyChange(PROP_PROPERTY, oldValue, y_str);
    }
    
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    
    public String calculate() throws SAXException, IOException, TransformerException, TransformerException{
        float a = Float.parseFloat(a_str);
        float b = Float.parseFloat(b_str);
        float c = Float.parseFloat(c_str);
        float x = Float.parseFloat(x_str);
    
        float y = (float) (a * Math.pow(x, 2) + b * x + c);
        
        setY_str(String.valueOf(y));
        
        results_list.add(new Result(a_str, b_str, c_str, x_str, y_str));
          
        saveToHistory();
        
        return y_str;
    }
    
    public String randomize()
    {
        Random rd = new Random();
        
        setA_str(String.valueOf(rd.nextInt()));
        setB_str(String.valueOf(rd.nextInt()));
        setC_str(String.valueOf(rd.nextInt()));
        setX_str(String.valueOf(rd.nextInt()));
        
        return "";
    }

    private void saveToHistory() {       
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            
            //add elements to Document
            Element rootElement = doc.createElementNS("", "History");
            
            //append root element to document
            doc.appendChild(rootElement);
            
             //append first child element to root element
            for(int i=0; i<results_list.size(); i++)
            {
                
                rootElement.appendChild(getResult(doc, String.valueOf(i+1), 
                        results_list.get(i).getA(), 
                        results_list.get(i).getB(), 
                        results_list.get(i).getC(), 
                        results_list.get(i).getX(), 
                        results_list.get(i).getY()));
            }
            
             //for output to file, console
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            
            //for pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            
            //write to console or file
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(history_file));
            
            //write data
            transformer.transform(source, console);
            transformer.transform(source, file);
            System.out.println("DONE");

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    
    private static Node getResult(Document doc, String id, String a, String b, String c, String x,String y) {
        Element result = doc.createElement("Result");

        //set id attribute
        result.setAttribute("id", id);

        //create a element
        result.appendChild(getResultElements(doc, result, "a", a));

        //create b element
        result.appendChild(getResultElements(doc, result, "b", b));

        //create c element
        result.appendChild(getResultElements(doc, result, "c", c));

        //create x element
        result.appendChild(getResultElements(doc, result, "x", x));
        
        //create y element
        result.appendChild(getResultElements(doc, result, "y", y));

        return result;
    }

        //utility method to create text node
    private static Node getResultElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }
}
