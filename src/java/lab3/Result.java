/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab3;

/**
 *
 * @author baro
 */
public class Result {
    private String a;
    private String b;
    private String c;
    private String x;
    private String y;
    
    public String getA(){
        return a;
    }
    
    public String getB(){
        return b;
    }
    
    public String getC(){
        return c;
    }
    
    public String getX(){
        return x;
    }
    
    public String getY(){
        return y;
    }
    
    public Result(String a, String b, String c, String x, String y){
        this.a = a;
        this.b = b;
        this.c = c;
        this.x = x;
        this.y = y;
    }
}
