/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package jsf;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author baro
 */
@Named(value = "login")
@RequestScoped
public class login {

    private final String username_correct = "student";
    private final String passwd_correct = "wcy";
    
    private String username;
    private String passwd;
    
    public String getUsername(){
        return username;
    }
    
    public void setUsername(String username){
        this.username = username;
    }
    
    public String getPasswd(){
        return passwd;
    }
    
    public void setPasswd(String passwd){
        this.passwd = passwd;
    }
    
    public Boolean check(){
        return username.equals(username_correct) && passwd.equals(passwd_correct);
    }
    
    /**
     * Creates a new instance of login
     */
    public login() {
    }
    
}
