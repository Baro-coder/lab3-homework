<%-- 
    Document   : history
    Created on : 31 maj 2022, 18:00:59
    Author     : baro
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE html>
<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JEET_Lab_3.3 - history</title>
    </head>
    <body>
        
        <c:import url="WEB-INF/results_history.xml" var="base" />
        <x:parse doc="${base}" var="res" />
        
        <h:form id="back_form">
            <h:commandButton value="Back" action="retry" />
        </h:form>
        <br/>
        
        <h2>History:</h2>
        <ul>
            <x:forEach select="$res/history/result" var="resource">
                <li>
                    <x:out select="a"/>*<x:out select="x"/>^2 + 
                    <x:out select="b"/>*<x:out select="x"/> +
                    <x:out select="c"/> = <b><x:out select="y"/></b>
                </li>
            </x:forEach>
            <br><br>
        </ul>
                
    </body>
</html>
</f:view>