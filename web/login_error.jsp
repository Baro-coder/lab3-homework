<%-- 
    Document   : login_error
    Created on : 31 maj 2022, 17:11:47
    Author     : baro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html xmlns:h="http://xmlns.jcp.org/jsf/html" xmlns:f="http://xmlns.jcp.org/jsf/core">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JEET_Lab_3.3 - invalid credentials</title>
        </head>
        <body>
            <h1><h:outputText value="Invalid credentials"/></h1>
            
            <h:form id="error_form">
                
                <h:outputText value="Username or password is not correct!" />
                <br /><br />
                
                <h:commandButton value="Try again" action="retry" />
                
            </h:form>

        </body>
    </html>
</f:view>