<%-- 
    Document   : menu
    Created on : 31 maj 2022, 17:40:10
    Author     : baro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JEET_Lab_3.3 - menu</title>
        </head>
        <body>
            
            <h1><h:outputText value="Menu"/></h1>
            
            <h:form id="menu_form">
             
                <h:commandButton value="Manual input" action="manual" /><br/>
                <h:commandButton value="Random input" action="random" /><br/>
                <h:commandButton value="View History" action="history" /><br/><br/>
                <h:commandButton value="Logout" action="logout" /><br/>
                
            </h:form>
                
        </body>
    </html>
</f:view>
