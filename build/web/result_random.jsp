<%-- 
    Document   : result_random
    Created on : 31 maj 2022, 18:50:06
    Author     : baro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "x" uri = "http://java.sun.com/jsp/jstl/xml" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="5; URL=http://localhost:8080/Lab3_homework/faces/history.jsp" /> 
        <title>JEET_Lab_3.3 - result</title>
    </head>
    <body>
                
        <h2>Result</h2>
        <br/>
        
        <jsp:useBean id="dBean" class="lab3.DataBean" scope="session" />
        
        <%= dBean.randomize()%>
        
        <b>a = </b><jsp:getProperty name="dBean" property="a_str" /><br/>
        <b>b = </b><jsp:getProperty name="dBean" property="b_str" /><br/>
        <b>c = </b><jsp:getProperty name="dBean" property="c_str" /><br/>
        <b>x = </b><jsp:getProperty name="dBean" property="x_str" /><br/><br/>
        
        <b>y = </b>
        <jsp:getProperty name="dBean" property="a_str" /> * <jsp:getProperty name="dBean" property="x_str" />^2 + 
        <jsp:getProperty name="dBean" property="b_str" /> * <jsp:getProperty name="dBean" property="x_str" /> +
        <jsp:getProperty name="dBean" property="c_str" /> = <b><%= dBean.calculate()%></b>
        
    </body>
</html>
